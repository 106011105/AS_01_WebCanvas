// Init
canvas.style.cursor = "url(./img/unavail.cur), no-drop";

let mouseOriginPos = {
  x: 0,
  y: 0
};
let canvasArray = [];
let canvasStep = -1;

// Add Tool Button Event Listener
$('#brush').on('click', brush);
$('#radiate').on('click', radiate);
$('#circle-full').on('click', circle_full);
$('#circle').on('click', circle);
$('#square-full').on('click', square_full);
$('#square').on('click', square);
$('#tri-full').on('click', tri_full);
$('#tri').on('click', tri);
$('#eraser').on('click', eraser);
$('#type').on('click', type);
$('#upload').on('click', upload);
$('#dl').on('click', download);
$('#redo').on('click', redo);
$('#undo').on('click', undo);
$('#trash').on('click', trash);
$('#brush-size').on('change', changeBrushSize);


// Handle Tool Function
function brush() {
  canvas.style.cursor = "url(./img/Handwriting.cur), auto";
  addCanvasEvent('brush');
}

function radiate() {
  canvas.style.cursor = "url(./img/Handwriting.cur), auto";
  addCanvasEvent('radiate');
}

function circle_full() {
  canvas.style.cursor = "url(./img/pen.cur), auto";
  addCanvasEvent('circle_full');
}

function circle() {
  canvas.style.cursor = "url(./img/pen.cur), auto";
  addCanvasEvent('circle');
}

function square_full() {
  canvas.style.cursor = "url(./img/square.cur), auto";
  addCanvasEvent('square_full');
}

function square() {
  canvas.style.cursor = "url(./img/square.cur), auto";
  addCanvasEvent('square');
}

function tri_full() {
  canvas.style.cursor = "url(./img/tri.cur), auto";
  addCanvasEvent('tri_full');
}

function tri() {
  canvas.style.cursor = "url(./img/tri.cur), auto";
  addCanvasEvent('tri');
}

function eraser() {
  canvas.style.cursor = "url(./img/Eraser.cur), auto";
  addCanvasEvent('eraser');
}

function type() {
  canvas.style.cursor = "url(./img/select.cur), auto";
  addCanvasEvent('type');
}

function upload() {
  addCanvasEvent('upload');
}

function download() {
  document.getElementById('dl').href = canvas.toDataURL("image/png", 1.0);
}

function redo() {
  if (canvasArray.length - 1 > canvasStep) {
    let imgData = canvasArray[++canvasStep];
    ctx.putImageData(imgData, 0, 0);
  }
}

function undo() {
  if (canvasStep > 0) {
    let imgData = canvasArray[--canvasStep];
    ctx.putImageData(imgData, 0, 0);
  }
}

function trash() {
  ctx.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);
  canvasArray = [canvasArray[0]];
  canvasStep = 0;
}

function changeBrushSize(e) {
  brushSize = e.currentTarget.value;
}

// Upload Patten Global Var
let img, str, typeEvent = 0;

// Canvas Event
function addCanvasEvent(tool) {
  let cvs = $('#canvas');
  cvs.off();

  switch (tool) {
    case "brush":
      cvs.on('mousedown', cBrush);
      break;
    case "radiate":
      cvs.on('mousedown', cRadiate);
      break;
    case "circle_full":
      cvs.on('mousedown', cCircleFull);
      break;
    case "circle":
      cvs.on('mousedown', cCircle);
      break;
    case "square_full":
      cvs.on('mousedown', cSquareFull);
      break;
    case "square":
      cvs.on('mousedown', cSquare);
      break;
    case "tri_full":
      cvs.on('mousedown', cTriFull);
      break;
    case "tri":
      cvs.on('mousedown', cTri);
      break;
    case "eraser":
      cvs.on('mousedown', cErase);
      break;
    case "type":
      $('#alert2').css("display", "flex").fadeIn(400).delay(5000).fadeOut(400);
      if (typeEvent > 0) {
        let imgData = canvasArray[canvasStep];
        ctx.putImageData(imgData, 0, 0);
        body.removeEventListener('keydown', input);
      }
      typeEvent = 1;
      str = "Text";
      TextX = 50;
      TextY = 50;
      ctx.fillStyle = pickedColor;
      ctx.font = fontSize + ' ' + fontFamily;
      body.addEventListener('keydown', input);

      ctx.fillStyle = pickedColor;
      ctx.font = fontSize + ' ' + fontFamily;
      ctx.fillText(str, TextX, TextY);

      cvs.on('mousedown', cType);
      break;
    case "upload":
      $('#file').on('change', e => {
        $('#alert').css("display", "flex").fadeIn(400).delay(5000).fadeOut(400);
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = (event) => {
          img = new Image();
          img.src = event.target.result;
          img.onload = () => {
            ctx.drawImage(img, 0, 0);
          }
        }
        canvas.style.cursor = "url(./img/move.cur), auto";
        $('#file').off();
      });

      cvs.on('mousedown', cMove);
      break;
  }
  cvs.on('mouseup', () => {
    cvs.off('mousemove');
    if (tool !== 'upload' && tool !== 'type')
      canvasPush();
  });

  if (tool === 'upload') {
    cvs.on('dblclick', () => {
      cvs.off('mousemove');
      cvs.off('mousedown');
      canvasPush();
      ImgX = 0;
      ImgY = 0;
      canvas.style.cursor = "url(./img/unavail.cur), no-drop";
    });
  }

  cvs.on('mouseout', () => {
    cvs.off('mousemove');
  });
}

// Canvas Event Function
function cBrush(e) {
  mouseOriginPos = getMousePos(canvas, e);
  e.preventDefault();
  ctx.beginPath();
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.strokeStyle = pickedColor;
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
  });
}

function cRadiate(e) {
  mouseOriginPos = getMousePos(canvas, e);
  e.preventDefault();
  ctx.beginPath();
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.fillStyle = pickedColor;
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.arc(mousePos.x, mousePos.y, brushSize*0.1, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
  });
}

function cErase(e) {
  mouseOriginPos = getMousePos(canvas, e);
  e.preventDefault();
  ctx.beginPath();
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.strokeStyle = "#fff";
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
  });
}

function cCircleFull(e) {
  mouseOriginPos = getMousePos(canvas, e);
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.fillStyle = pickedColor;
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.beginPath();
    ctx.arc(mouseOriginPos.x, mouseOriginPos.y, len(mouseOriginPos, mousePos), 0, Math.PI * 2, true);
    ctx.fill();
    ctx.closePath();
  });
}

function cCircle(e) {
  mouseOriginPos = getMousePos(canvas, e);
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.strokeStyle = pickedColor;
    ctx.beginPath();
    ctx.arc(mouseOriginPos.x, mouseOriginPos.y, len(mouseOriginPos, mousePos), 0, Math.PI * 2, true);
    ctx.stroke();
    ctx.closePath();
  });
}

function cSquareFull(e) {
  mouseOriginPos = getMousePos(canvas, e);
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.fillStyle = pickedColor;
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.fillRect(mouseOriginPos.x, mouseOriginPos.y, mousePos.x - mouseOriginPos.x, mousePos.y - mouseOriginPos.y);
  });
}

function cSquare(e) {
  mouseOriginPos = getMousePos(canvas, e);
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.strokeStyle = pickedColor;
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.strokeRect(mouseOriginPos.x, mouseOriginPos.y, mousePos.x - mouseOriginPos.x, mousePos.y - mouseOriginPos.y);
  });
}

function cTriFull(e) {
  mouseOriginPos = getMousePos(canvas, e);
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.fillStyle = pickedColor;
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.beginPath();
    ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.lineTo(mouseOriginPos.x * 2 - mousePos.x, mousePos.y);
    ctx.fill();
    ctx.closePath();
  });
}

function cTri(e) {
  mouseOriginPos = getMousePos(canvas, e);
  ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    ctx.lineWidth = brushSize;
    ctx.lineCap = 'round';
    ctx.strokeStyle = pickedColor;
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.beginPath();
    ctx.moveTo(mouseOriginPos.x, mouseOriginPos.y);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.lineTo(mouseOriginPos.x * 2 - mousePos.x, mousePos.y);
    ctx.lineTo(mouseOriginPos.x, mouseOriginPos.y);
    ctx.stroke();
    ctx.closePath();
  });
}

let ImgX = 0,
  ImgY = 0;

function cMove(e) {
  mouseOriginPos = getMousePos(canvas, e);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.drawImage(img, mousePos.x - mouseOriginPos.x + ImgX, mousePos.y - mouseOriginPos.y + ImgY);
  });

  $('#canvas').on('mouseup', function record(evt) {
    let mousePos = getMousePos(canvas, evt);
    ImgX += mousePos.x - mouseOriginPos.x;
    ImgY += mousePos.y - mouseOriginPos.y;
    $('#canvas').off('mouseup', record);
  });
}

let TextX = 50;
TextY = 50;

function cType(e) {
  mouseOriginPos = getMousePos(canvas, e);
  $('#canvas').on('mousemove', (evt) => {
    let mousePos = getMousePos(canvas, evt);
    let imgData = canvasArray[canvasStep];
    ctx.putImageData(imgData, 0, 0);
    ctx.fillText(str, mousePos.x - mouseOriginPos.x + TextX, mousePos.y - mouseOriginPos.y + TextY);
  });

  $('#canvas').on('mouseup', function record(evt) {
    let mousePos = getMousePos(canvas, evt);
    TextX += mousePos.x - mouseOriginPos.x;
    TextY += mousePos.y - mouseOriginPos.y;
    $('#canvas').off('mouseup', record);
  });
}

function input(e) {
  if (e.getModifierState(e.key) == false) {
    let key = e.key;
    if (key == 'Backspace') {
      str = str.slice(0, str.length - 1);
      let imgData = canvasArray[canvasStep];
      ctx.putImageData(imgData, 0, 0);
      ctx.fillText(str, TextX, TextY);
    } else if (key == 'Enter') {
      str = "Text";
      canvasPush();
      canvas.style.cursor = "url(./img/unavail.cur), no-drop";
      TextX = 50;
      TextY = 50;
      body.removeEventListener('keydown', input);
      $('#canvas').off('mousedown', cType);
    } else if (key.length === 1) {
      str += key;
      let imgData = canvasArray[canvasStep];
      ctx.putImageData(imgData, 0, 0);
      ctx.fillText(str, TextX, TextY);
    }
  }
}

// History
function canvasPush() {
  if (canvasStep >= -1) {
    let imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    canvasArray[++canvasStep] = imgData;
  }

}
canvasPush();

// Calculate length
function len(a, b) {
  return Math.sqrt(Math.pow((a.x - b.x), 2) + Math.pow((a.y - b.y), 2));
}