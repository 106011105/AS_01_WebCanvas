let font = document.querySelector('#font-family');
let fonttype = 1;

function changefont() {
  fonttype = fonttype < 5 ? fonttype + 1 : 1;
  switch (fonttype) {
    case 1:
      font.style.fontFamily = `'Times New Roman', Times, serif`;
      break;
    case 2:
      font.style.fontFamily = `'Margarine'`;
      break;
    case 3:
      font.style.fontFamily = `'Baloo Chettan'`;
      break;
    case 4:
      font.style.fontFamily = `'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif`;
      break;
    case 5:
      font.style.fontFamily = `'Lobster'`;
      break;
    default:
      break;
  }
  fontFamily = font.style.fontFamily;
}

font.addEventListener('click', changefont);

let size = document.querySelector('#font-size');
let textsize = 1;

function changesize() {
  textsize = textsize < 5 ? textsize + 1 : 1;
  switch (textsize) {
    case 1:
      size.style.fontSize = '2rem';
      break;
    case 2:
      size.style.fontSize = '2.4rem';
      break;
    case 3:
      size.style.fontSize = '2.8rem';
      break;
    case 4:
      size.style.fontSize = '1.2rem';
      break;
    case 5:
      size.style.fontSize = '1.6rem';
      break;
    default:
      break;
  }
  fontSize = size.style.fontSize;
}

size.addEventListener('click', changesize);