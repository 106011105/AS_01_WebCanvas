let body = document.querySelector('body');

// Color Picker
const palette = document.getElementById('palette');
const pctx = palette.getContext('2d');
let pickedColor = "rgba(0, 0, 0, 1)";

// Brush
let brushSize = 1;

// Font Picker
let fontFamily = "'Times New Roman', Times, serif";
let fontSize = "1rem";

// Canvas
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
// let x = 0, y = 0;
// let tool = "brush";

function getMousePos(canvas, event) {
  let rect = canvas.getBoundingClientRect();
  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top
  }
}