let picking = document.querySelector('#picking-color');
let picked = document.querySelector('#picked-color');

for (var i = 0;i < 360;i++) {
  pctx.fillStyle = '	hsl('+i+', 100%, 50%)';
  pctx.fillRect(0, i*0.67, 50, 1);
}

function pick(event) {
  var x = event.layerX;
  var y = event.layerY;
  var pixel = pctx.getImageData(x, y, 1, 1);
  var data = pixel.data;
  var pickingColor = 'rgba(' + data[0] + ', ' + data[1] +
             ', ' + data[2] + ', ' + (data[3] / 255) + ')';
  picking.style.background = pickingColor;
}

palette.addEventListener('mousemove', pick);
palette.addEventListener('click', () => {
  picked.style.background = picking.style.background
  pickedColor = picked.style.background;
});
palette.addEventListener('mouseout', () => picking.style.background = 'none');