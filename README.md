# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

### Brush Tool

| 工具 | 說明 |
| -------- | -------- |
| <img src="left.png"></img>| 1. 第一個是下拉式選單可調整筆刷與橡皮擦大小<br><br>2. 第二個是一般筆刷<br><br>3. 第三個是特殊筆刷，可以畫出放射狀線條<br><br>4. 第四個是實心圓形筆刷<br><br>5. 第五個是空心圓型筆刷，可以製造多層同心圓效果<br><br>6. 第六個是實心方形筆刷<br><br>7. 第七個是空心方型筆刷<br><br>8. 第八個是實心三角形筆刷<br><br>9. 第九個是空心三角型筆刷<br><br><br><br><br><br><br><br><br><br><br> |                     
                      
### Tool

| 工具 | 說明 |
| -------- | -------- |
| <img src="right.png"></img>| 1. 第一個是橡皮擦<br><br>2. 第二個是打字機，點擊後在畫布上產生Text字樣，可以直接輸入文字，也可以拖移，按下Enter後確認保存<br><br>3. 第三個是上傳圖片，可以拖移，滑鼠雙擊後確定位置後儲存<br><br>4. 第四個是下載<br><br>5. 第五個是Redo，可以重做Undo的步驟<br><br>6. 第六個Undo，可以返回上個步驟的狀態<br><br>7. 第七個是Reset，畫布重回空白狀態，紀錄的步驟也將刪除<br><br><br><br><br><br><br><br><br><br><br><br><br><br> |                     

### Color & Font Picker

| 工具 | 說明 |
| -------- | -------- |
| <img src="color.png"></img>| 1. 第一個是顏色選擇器，點擊後確認顏色<br><br>2. 第二個是顏色預覽，當滑鼠在顏色選擇器上移動時跟著改變顏色<br><br>3. 第三個是目前選擇的顏色<br><br>4. 第四個是打字機的字體選擇，點擊可以切換字體<br><br>5. 第五個是打字機的字體大小選擇，點擊可以切換字體大小<br><br><br><br><br><br><br><br><br><br><br><br><br><br> |                     

    
    

